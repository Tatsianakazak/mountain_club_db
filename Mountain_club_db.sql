CREATE TABLE climbers (
  climbers_id INTEGER PRIMARY KEY NOT NULL,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL,
  address TEXT NOT NULL,
  equipment TEXT,
  gender TEXT CHECK (gender IN ('Male', 'Female', 'Other')) 
);


CREATE TABLE countries (
  country_id INTEGER PRIMARY KEY NOT NULL,
  name_country TEXT NOT NULL
);

CREATE TABLE areas (
  area_id INTEGER PRIMARY KEY NOT NULL,
  name_area TEXT NOT NULL
);

CREATE TABLE equipment (
  equipment_id INTEGER PRIMARY KEY NOT NULL,
  name_equipment TEXT NOT NULL,
  measured_value INTEGER CHECK (measured_value >= 0)
);

CREATE TABLE climbs_incident (
  incident_id INTEGER PRIMARY KEY NOT NULL,
  incident_type TEXT NOT NULL
);

CREATE TABLE climbs_complexity (
  complexity_id INTEGER PRIMARY KEY NOT NULL,
  complexity TEXT NOT NULL
);

CREATE TABLE climbs_status (
  status_id INTEGER PRIMARY KEY NOT NULL,
  status TEXT NOT NULL
);

CREATE TABLE instructors (
  instructor_id INTEGER PRIMARY KEY NOT NULL,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL
);
CREATE TABLE mountains (
  mountain_id INTEGER PRIMARY KEY NOT NULL,
  name_mountain TEXT NOT NULL,
  fk_country_id INTEGER NOT NULL,
  fk_area_id INTEGER NOT NULL,
FOREIGN KEY (fk_country_id) REFERENCES countries (country_id),
FOREIGN KEY (fk_area_id) REFERENCES areas(area_id)
);
CREATE TABLE climbs (
    climbs_id INTEGER PRIMARY KEY NOT NULL,
    start_date DATE NOT NULL DEFAULT CURRENT_DATE CHECK (start_date > '2000-01-01'), 
    end_date DATE NOT NULL DEFAULT CURRENT_DATE CHECK (end_date > '2000-01-01'),
    fk_climber_id INTEGER ,
    fk_mountain_id INTEGER ,
    fk_complexity_id INTEGER ,
    fk_status_id INTEGER ,
    fk_instructor_id INTEGER,
    fk_incident_id INTEGER,
    duration INTEGER GENERATED ALWAYS AS (end_date - start_date) STORED,
	FOREIGN KEY (fk_climber_id) REFERENCES climbers(climbers_id),
CONSTRAINT fk_mountain_id FOREIGN KEY (fk_mountain_id) REFERENCES mountains(mountain_id),
CONSTRAINT fk_complexity_id FOREIGN KEY (fk_complexity_id) REFERENCES climbs_complexity(complexity_id),
CONSTRAINT fk_status_id FOREIGN KEY (fk_status_id) REFERENCES climbs_status(status_id),
CONSTRAINT fk_instructor_id FOREIGN KEY (fk_instructor_id) REFERENCES instructors(instructor_id),
CONSTRAINT fk_incident_id FOREIGN KEY (fk_incident_id) REFERENCES climbs_incident(incident_id)
);
INSERT INTO areas (area_id, name_area)
	VALUES
	(1,'Himalayas'),
	(2,'Eastern Rift Mountains'),
	(3, 'Cascade Range'),
  (4,'Pennine Alps'),
  (5, 'Fuji-Hakone-Izu'),
  (6,'Alaska Range'),
  (7,'Graian Alps'),
  (8, 'Andes'),
  (9, 'Caucasus Mountains'),
  (10, 'Karakoram'),
  (11,'Alaska Range'),
  (12,'Snowy Mountains'),
  (13,'Sierra Nevada'),
  (14, 'White Mountains'),
  (15, 'Saint Elias Mountains'),
  (16, 'Mount Kenya'),
  (17, 'Olympus Range'),
  (18,'Campanian volcanic arc'),
  (19, 'Cascade Range'),
  (20, 'Sicily');
INSERT INTO countries (country_id, name_country)
	VALUES
(1, 'Nepal'),
  (2, 'Tanzania'),
  (3, 'United States'),
  (4, 'Switzerland'),
  (5, 'Japan'),
  (6, 'United States'),
  (7, 'France'),
  (8, 'Argentina'),
  (9,  'Russia'),
	(10, 'Pakistan'),
  (11, 'United States'),
  (12,  'Australia'),
  (13, 'United States'),
  (14, 'United States'),
  (15, 'Canada'),
  (16, 'Kenya'),
  (17, 'Greece'),
  (18,  'Italy'),
  (19,'United States'),
  (20, 'Italy');

INSERT INTO mountains (mountain_id, name_mountain, fk_country_id, fk_area_id)
VALUES
  (1, 'Mount Everest', 1, 1),
  (2, 'Mount Kilimanjaro', 2, 2),
  (3, 'Mount Rainier', 3, 3),
  (4, 'Matterhorn', 4, 4),
  (5, 'Mount Fuji', 5, 5),
  (6, 'Denali', 6, 6),
  (7, 'Mont Blanc', 7, 7),
  (8, 'Aconcagua', 8, 8),
  (9, 'Mount Elbrus', 9, 9),
  (10, 'K2', 10, 10),
  (11, 'Mount McKinley', 11, 11),
  (12, 'Mount Kosciuszko', 12, 12),
  (13, 'Mount Whitney', 13, 13),
  (14, 'Mount Washington', 14, 14),
  (15, 'Mount Logan', 15, 15),
  (16, 'Mount Kenya', 16, 16),
  (17, 'Mount Olympus', 17, 17),
  (18, 'Mount Vesuvius', 18, 18),
  (19, 'Mount Hood', 19, 19),
  (20, 'Mount Etna', 20, 20);

INSERT INTO instructors (instructor_id, first_name, last_name)
VALUES
  (1, 'John', 'Smith'),
  (2, 'Emily', 'Johnson'),
  (3, 'Michael', 'Williams'),
  (4, 'Jessica', 'Jones'),
  (5, 'Christopher', 'Brown'),
  (6, 'Jennifer', 'Davis'),
  (7, 'Matthew', 'Miller'),
  (8, 'Elizabeth', 'Wilson'),
  (9, 'Daniel', 'Moore'),
  (10, 'Patricia', 'Taylor'),
  (11, 'David', 'Anderson'),
  (12, 'Linda', 'Thomas'),
  (13, 'James', 'Jackson'),
  (14, 'Barbara', 'White'),
  (15, 'Robert', 'Harris'),
  (16, 'Mary', 'Martin'),
  (17, 'William', 'Thompson'),
  (18, 'Margaret', 'Garcia'),
  (19, 'Charles', 'Martinez'),
  (20, 'Sarah', 'Robinson');



INSERT INTO equipment (equipment_id, name_equipment)
VALUES
  (1, 'Climbing Rope'),
  (2, 'Harness'),
  (3, 'Helmet'),
  (4, 'Carabiners'),
  (5, 'Crampons'),
  (6, 'Ice Axe'),
  (7, 'Belay Device'),
  (8, 'Quickdraws'),
  (9, 'Tent'),
  (10, 'Sleeping Bag'),
  (11, 'Backpack'),
  (12, 'GPS Device'),
  (13, 'First Aid Kit'),
  (14, 'Headlamp'),
  (15, 'Climbing Shoes'),
  (16, 'Water Bottle'),
  (17, 'Map and Compass'),
  (18, 'Sunscreen'),
  (19, 'Trekking Poles'),
  (20, 'Portable Stove');

INSERT INTO climbs_incident (incident_id, incident_type)
VALUES
  (1, 'Fall'),
  (2, 'Injury'),
  (3, 'Equipment Malfunction'),
  (4, 'Weather Conditions'),
  (5, 'Getting Lost'),
  (6, 'Avalanche'),
  (7, 'Exhaustion'),
  (8, 'Rockfall'),
  (9, 'Altitude Sickness'),
  (10, 'Animal Encounter'),
  (11, 'Fatigue'),
  (12, 'Dehydration'),
  (13, 'Hypothermia'),
  (14, 'Heat Stroke'),
  (15, 'Dizziness'),
  (16, 'Frostbite'),
  (17, 'Sunburn'),
  (18, 'Cramp'),
  (19, 'Anxiety'),
  (20, 'Panic Attack');

INSERT INTO climbs_status (status_id, status)
VALUES
  (1, 'Planned'),
  (2, 'In Progress'),
  (3, 'Completed');

INSERT INTO Climbers (climbers_id, first_name, last_name, address, equipment, gender)
VALUES
  (1, 'John', 'Doe', '123 Main St', 'Harness, Rope', 'Male'),
  (2, 'Jane', 'Smith', '456 Oak Ave', 'Crampons, Ice Axe', 'Female'),
  (3, 'Michael', 'Johnson', '789 Elm St', 'Helmet, Carabiners', 'Male'),
  (4, 'Emily', 'Brown', '101 Maple Ave', 'Tent, Sleeping Bag', 'Female'),
  (5, 'David', 'Davis', '234 Pine St', 'GPS, Compass', 'Male'),
  (6, 'Emma', 'Wilson', '567 Cedar Ave', 'Headlamp, Backpack', 'Female'),
  (7, 'James', 'Jones', '890 Oak St', 'Water Bottle, Sunglasses', 'Male'),
  (8, 'Olivia', 'Taylor', '112 Birch Ave', 'Map, First Aid Kit', 'Female'),
  (9, 'William', 'White', '345 Elm St', 'Trekking Poles, Gloves', 'Male'),
  (10, 'Ava', 'Martinez', '678 Pine Ave', 'Hiking Boots, Jacket', 'Female'),
  (11, 'Alexander', 'Anderson', '901 Oak St', 'Binoculars, Sunscreen', 'Male'),
  (12, 'Sophia', 'Lee', '234 Maple Ave', 'Emergency Whistle, Camera', 'Female'),
  (13, 'Daniel', 'Clark', '567 Birch St', 'Trail Food, Multi-tool', 'Male'),
  (14, 'Isabella', 'Rodriguez', '890 Cedar Ave', 'Backpack Cover, Rain Jacket', 'Female'),
  (15, 'Matthew', 'Lewis', '123 Pine St', 'Water Purifier, Trekking Shoes', 'Male'),
  (16, 'Mia', 'Hernandez', '456 Elm Ave', 'Sleeping Pad, Cooking Stove', 'Female'),
  (17, 'Ethan', 'Walker', '789 Cedar St', 'Emergency Blanket, Fire Starter', 'Male'),
  (18, 'Amelia', 'Young', '101 Birch Ave', 'Compass, Rope', 'Female'),
  (19, 'Benjamin', 'King', '234 Oak St', 'Torch, Tent Pegs', 'Male'),
  (20, 'Charlotte', 'Wright', '567 Maple Ave', 'Hiking Socks, Water Filter', 'Female');

INSERT INTO climbs_complexity (complexity_id, complexity)
VALUES
  (1, 'Low'),
  (2, 'Medium'),
  (3, 'High');

INSERT INTO climbs (climbs_id, start_date, end_date, fk_climber_id, fk_mountain_id, fk_complexity_id, fk_status_id, fk_instructor_id, fk_incident_id)
VALUES
  (1, '2023-01-05', '2023-01-07', 1, 1, 2, 3, 1, 1),
  (2, '2023-02-10', '2023-02-12', 2, 2, 1, 2, 2, 2),
  (3, '2023-03-15', '2023-03-18', 3, 3, 3, 1, 3, 3),
  (4, '2023-04-20', '2023-04-22', 4, 4, 2, 3, 4, 4),
  (5, '2023-05-25', '2023-05-27', 5, 5, 1, 2, 5, 5),
  (6, '2023-06-30', '2023-07-02', 6, 6, 3, 1, 6, 6),
  (7, '2023-08-05', '2023-08-07', 7, 7, 2, 3, 7, 7),
  (8, '2023-09-10', '2023-09-12', 8, 8, 1, 2, 8, 8),
  (9, '2023-10-15', '2023-10-17', 9, 9, 3, 1, 9, 9),
  (10, '2023-11-20', '2023-11-22', 10, 10, 2, 3, 10, 10),
  (11, '2023-12-25', '2023-12-27', 11, 11, 1, 2, 11, 11),
  (12, '2024-01-01', '2024-01-03', 12, 12, 3, 1, 12, 12),
  (13, '2024-02-05', '2024-02-07', 13, 13, 2, 3, 13, 13),
  (14, '2024-03-10', '2024-03-12', 14, 14, 1, 2, 14, 14),
  (15, '2024-04-15', '2024-04-17', 15, 15, 3, 1, 15, 15),
  (16, '2024-05-20', '2024-05-22', 16, 16, 2, 3, 16, 16),
  (17, '2024-06-25', '2024-06-27', 17, 17, 1, 2, 17, 17),
  (18, '2024-07-30', '2024-08-01', 18, 18, 3, 1, 18, 18),
  (19, '2024-09-04', '2024-09-06', 19, 19, 2, 3, 19, 19),
  (20, '2024-10-10', '2024-10-12', 20, 20, 1, 2, 20, 20);

ALTER TABLE climbs
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE climbers
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE mountains
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE countries
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;


ALTER TABLE areas
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE equipment
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE climbs_incident
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE climbs_complexity
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE climbs_status
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE instructors
ADD COLUMN record_ts DATE DEFAULT CURRENT_DATE;

SELECT * FROM climbs WHERE record_ts IS NULL;

SELECT * FROM climbers WHERE record_ts IS NULL;

SELECT * FROM mountains WHERE record_ts IS NULL;

SELECT * FROM countries WHERE record_ts IS NULL;

SELECT * FROM areas WHERE record_ts IS NULL;

SELECT * FROM equipment WHERE record_ts IS NULL;

SELECT * FROM instructors WHERE record_ts IS NULL;

SELECT * FROM climbs_status WHERE record_ts IS NULL;

SELECT * FROM climbs_complexity WHERE record_ts IS NULL;

SELECT * FROM climbs_incident WHERE record_ts IS NULL;