-- DROP TABLE  Orders, Products, Order_Items;

CREATE TABLE Orders  (
    o_id serial PRIMARY KEY,
    order_date date
);

CREATE TABLE Products  (
    p_name text PRIMARY KEY,
    price money
);
CREATE TABLE Order_Items  (
    order_id int,
    product_name text,
    amount numeric(7,2),
    PRIMARY KEY (order_id, product_name),
    FOREIGN KEY (order_id) REFERENCES Orders(o_id),
    FOREIGN KEY (product_name) REFERENCES Products(p_name)
);

INSERT INTO Orders (order_date) VALUES
('2024-05-05'), 
('2024-04-15'); 

INSERT INTO Products (p_name, price) VALUES
('p1', '$19.99'),  
('p2', '$29.99'); 

INSERT INTO Order_Items (order_id, product_name, amount) VALUES
(1, 'p1', 1.00),  
(1, 'p2', 1.00);  

INSERT INTO Order_Items (order_id, product_name, amount) VALUES
(2, 'p1', 5.00),  
(2, 'p2', 3.00);  


ALTER TABLE Orders
ALTER COLUMN order_date SET NOT NULL;

ALTER TABLE Products
ALTER COLUMN price SET NOT NULL;

ALTER TABLE Order_Items
ALTER COLUMN order_id SET NOT NULL,
ALTER COLUMN product_name SET NOT NULL,
ALTER COLUMN amount SET NOT NULL;


ALTER TABLE Order_Items DROP CONSTRAINT order_items_product_name_fkey;

ALTER TABLE Products DROP CONSTRAINT products_pkey;

ALTER TABLE Products ADD COLUMN p_id serial;
ALTER TABLE Products ADD PRIMARY KEY (p_id);

ALTER TABLE Order_Items ADD COLUMN product_id int;
ALTER TABLE Order_Items ADD CONSTRAINT order_items_product_id_fkey FOREIGN KEY (product_id) REFERENCES Products(p_id);

UPDATE Order_Items SET product_id = (SELECT p_id FROM Products WHERE Products.p_name = Order_Items.product_name);

ALTER TABLE Products
ADD CONSTRAINT unique_p_name UNIQUE (p_name);


ALTER TABLE Order_Items
ADD COLUMN price money,
ADD COLUMN total money;

UPDATE Order_Items
SET price = (
    SELECT price
    FROM Products
    WHERE Products.p_id = Order_Items.product_id
);

UPDATE Order_Items
SET price = 0
WHERE price IS NULL;

UPDATE Order_Items
SET total = amount * price;

ALTER TABLE Order_Items
ADD CONSTRAINT total_check CHECK (total = amount * price);

UPDATE Products
SET p_name = 'product1'
WHERE p_name = 'p1';

DELETE FROM Order_Items
WHERE product_name = 'p2' AND order_id = 1;

DELETE FROM Order_Items
WHERE order_id = 2;

DELETE FROM Orders
WHERE o_id = 2;


UPDATE Products
SET price = 5
WHERE p_name = 'product1';

UPDATE Order_Items
SET price = 5
WHERE product_name = 'product1';

UPDATE Order_Items
SET total = amount * price
WHERE product_name = 'product1';

INSERT INTO Orders (order_date)
VALUES (CURRENT_DATE);


INSERT INTO Orders (order_date)
VALUES (CURRENT_DATE);


INSERT INTO Orders (order_date)
VALUES (CURRENT_DATE);

INSERT INTO Order_Items (order_id, product_name, amount, price, total)
SELECT 
    (SELECT MAX(o_id) FROM Orders),
    'product1',
    3,
    (SELECT price FROM Products WHERE p_name = 'product1'),
    3 * (SELECT price FROM Products WHERE p_name = 'product1')
FROM Products
WHERE p_name = 'product1';

